# Ember App Kit [![Build Status](https://travis-ci.org/stefanpenner/ember-app-kit.png?branch=master)](https://travis-ci.org/stefanpenner/ember-app-kit)

Ember App Kit aims to be the foundation for ambitious web applications built with Ember. It will soon be replaced by an executable [ember-cli](https://github.com/stefanpenner/ember-cli) which dramatically improves buildtimes (via broccoli) and provides sane-upgrade paths, feel free to check that project out. We intend to provide a sensible upgrade path.

This project has been extracted out of several real world applications and is actively used. Currently it covers the basics fairly well, but much still needs to be done. As we learn and as more contributors join in it continues to evolve. If you encounter any bugs, clunky features or missing documentation, just submit an issue and we'll respond ASAP.

At the very least, it helps setup your Ember.js applications directory structure.

We welcome ideas and experiments.

## Getting Started

* [Project Documentation Site](http://stefanpenner.github.io/ember-app-kit/)
* [Getting Started Guide](http://stefanpenner.github.io/ember-app-kit/guides/getting-started.html)
* [ember-app-kit-todos](https://github.com/stefanpenner/ember-app-kit-todos) - the Emberjs [todos](http://emberjs.com/guides/getting-started/) using Ember App Kit 
* [ember-app-kit-bloggr](https://github.com/pixelhandler/ember-app-kit-example-with-bloggr-client) - bloggr demo
* *Safari Books Online Blog* - [Introduction to Ember App Kit](http://blog.safaribooksonline.com/2013/09/18/ember-app-kit/) for more experienced Ember developers by @mixonic
* *Ember Sherpa* - [Introduction to Ember App Kit](http://embersherpa.com/articles/introduction-to-ember-app-kit/) for those who are new to the Grunt workflow by @taras 


## Features

- Sane project structure
- ES6 module transpiler support (easy, future-proof modules)
- Module system-aware resolver (see [Referencing views](https://github.com/stefanpenner/ember-app-kit/wiki/Referencing-Views) and [Using Ember loaders](https://github.com/stefanpenner/ember-app-kit/wiki/Using-Ember-loaders))
- Transparent project compilation & minification for easy deploys via [Grunt](http://gruntjs.com/)
- Package management via [Bower](https://github.com/bower/bower)
- Optional support for CoffeeScript, SASS, LESS or Stylus
- Testing via QUnit, Ember Testing and Testem (with examples)
- Linting via JSHint (including module syntax)
- Catch-all `index.html` for easy reloading of pushState router apps
- Generators via [Loom](https://github.com/cavneb/loom-generators-ember-appkit) (to generate routes, controllers, etc.)

## Future Goals

- Source maps for transpiled modules
- Easier to install 3rd party packages
- Faster, more intelligent builds

Think anything else is missing? Feel free to open an issue (or, even better, a PR)! Discussion and feedback is always appreciated.

## Special Thanks

Some ideas in ember-app-kit originated in work by Yapp Labs (@yapplabs) with McGraw-Hill Education Labs (@mhelabs) on [yapplabs/glazier](https://github.com/yapplabs/glazier). Thanks to Yapp and MHE for supporting the Ember ecosystem!

## License

Copyright 2013 by Stefan Penner and Ember App Kit Contributors, and licensed under the MIT License. See included
[LICENSE](/stefanpenner/ember-app-kit/blob/master/LICENSE) file for details.


============================================================================================



DATE | 3/18/2014 (JSON Models)

================== APP

    Login / Logout

    Manage

      Website 
        Add
        Edit

      Place 
        Add
        Edit
   
      Agency 
        Add
        Edit

      Entity 
        Add
        Edit
   
      Resource 
        Add
        Edit
   
      Registered Agent 
        Add
        Edit
   
      Product 
        Add
        Edit
   
      Document 
        Client
          Category
          Uploads
          

      Client 
        Resource
        Document
        Reminder
        Add Service
        Account Detail

        Local Office
          Upload Document

================== NODE|BOWER SETUP

(new machine)
sudo npm install -g grunt-cli
sudo npm install -g bower
sudo npm install -g less
sudo npm install --save-dev grunt-contrib-less

(generators)
sudo npm install -g loom-generators-ember-appkit --save

(deploy tools)
sudo npm install -g grunt-usemin
sudo npm install -g grunt-rev

================== EAK

$ (clone appkit)
  cd ~/dev, git clone https://github.com/stefanpenner/ember-app-kit.git {project-name}, cd ~/dev/{project-name}

$ (remove git, create own)
  rm -rf .git, git init, git remote add origin https://{username}@bitbucket.org/{username}}/{projectname}.git

$ (install npm)
  npm install

$ (start server)
  grunt server, chrome -> http://localhost:8000/

================== OSX

sudo apachectl start (apache)
ln -s /path to sublime/ sudo !! (symlink from /usr/local/bin)
ember inspector - command+option+j

================== BITBUCKET

BitBUCKET
  https://arllcagent@bitbucket.org/

Bitbucket path convention:
  https://arllcagent@bitbucket.org/arllcagent/corptools/admin.git
  https://arllcagent@bitbucket.org/corptools/corptools-backend.git
  https://arllcagent@bitbucket.org/corptools/corptools-frontend.git

  (setup)
  git remote add origin https://arllcagent@bitbucket.org/arllcagent/corptools.git
  git add .
  git status
  git commit -m "initial commit"
  git push origin master

  (commit)
  git add . || --all (to delete)
  git commit -m "readme update"
  git push origin master

  (checkout)
  git pull origin master (checkout)

  git remote add origin 

  (checkin)
  cd /local-repo
  git remote add origin https://arllcagent@bitbucket.org/arllcagent/appkit.git
  git push -u origin --all # pushes up the repo and its refs for the first time
  git push -u origin --tags # pushes up any tags


================== LINKS

installs:
  http://nodejs.org/download/

reading:
  http://emberjs.com/api/
  http://bower.io/
  http://emberjs.com/guides/
  http://getbootstrap.com/css/
  http://stackoverflow.com/questions/15624593/inserting-a-model-property-into-an-img-element-url-in-ember
  http://stackoverflow.com/questions/20684879/ember-and-external-js-scripts
  http://stackoverflow.com/questions/15017405/using-jquery-in-ember-app
  http://embersherpa.com/articles/crud-example-app-without-ember-data/
  http://q42.nl/blog/post/35203391115/debug-sass-and-less-in-webkit-inspector-and-save-css-cha
  https://www.openshift.com/blogs/day-1-bower-manage-your-client-side-dependencies
  http://www.abeautifulsite.net/blog/2013/08/whipping-file-inputs-into-shape-with-bootstrap-3/
  http://labs.abeautifulsite.net/demos/bootstrap-file-inputs/
  http://stackoverflow.com/questions/15094667/compile-less-files-with-grunt-contrib-less-wont-work
  http://ericnishio.com/blog/compile-less-files-with-grunt
  http://discuss.emberjs.com/t/best-practice-for-using-bound-ember-templates-along-with-bootstrap-components/3801
  http://readwrite.com/2013/09/30/understanding-github-a-journey-for-beginners-part-1
  http://matthewlehner.net/ember-js-routing-and-views/
  http://brew.sh/
  http://iamstef.net/ember-app-kit/
  http://www.thesoftwaresimpleton.com/blog/2014/03/11/cp-all-keys/?utm_content=buffer1d0a0&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer
  http://handlebarsjs.com/util.html
  https://github.com/dockyard/ember-easyForm

================== GITS

https://github.com/emberjs/website/trunk/source/guides/
https://github.com/cavneb/loom-generators-ember-appkit
https://github.com/gruntjs/grunt-contrib-less
https://github.com/stefanpenner/ember-app-kit
https://github.com/stefanpenner/ember-app-kit/blob/master/app/index.html#L24
https://gist.github.com/stefanpenner/6d8e33ed5f62965b5559 (bower)

================== DEPLOY

grunt build:dist
grunt server:dist

(push to bb)

git add . 
git commit -m "mesesage"
git push origin master

chrome -> http://corporatetools.com/

================== NOTES

for eak, .less files live adjacent to /styles/.css, (grunt-contrib-less auto reloads)
  bower install bootstrap --save
  npm install generator-ember-less







