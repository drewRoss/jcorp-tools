export default Ember.Route.extend({
	model: function() {
		console.log("~~~~~~~~~~~service list~~~~~~~~~~~~");

		// Todo: Move to JSON call /public/json/services.json
		var serviceArr = [
			{"id": "1", "routeName":"website",					"routeLabel":"website manager"},
			{"id": "2", "routeName":"agency",						"routeLabel":"place manager"},
			{"id": "3", "routeName":"document",					"routeLabel":"agency manager"},
			{"id": "4", "routeName":"entity",						"routeLabel":"resource manager"},
			{"id": "5", "routeName":"place",						"routeLabel":"registered agent manager"},
			{"id": "6", "routeName":"registered-agent",	"routeLabel":"product manager"},
			{"id": "7", "routeName":"resource",					"routeLabel":"document manager"}
		];

		return serviceArr;

	}
});
