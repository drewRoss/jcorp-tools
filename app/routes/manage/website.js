export default Ember.Route.extend({

	model: function() {
		console.log("~~~~~~~~~~~website list~~~~~~~~~~~~");
		
		/*
	  	model: function() {
		    console.log("~~~~~~~~~~~~~ MANAGE ~~~~~~~~~~~~~~~");
				var websiteObj = [];
				$.getJSON("/assets/json/websites.json").then(function(webDat) {
					console.log("~~~~~~~~~~~~~ JSON COMPLETED");
					webDat.websites.forEach(function(data) {
						console.log("~~~~~~~~~~~~~~~~ "+data.company_name);
						websiteObj.pushObject(data);
					});
				});
				return websiteObj;
	    }
		*/

		// Todo: Move to JSON call /public/json/websites.json
		var websites = [
        {
            id                  : 'nwra',
            company_name        : 'Northwest Registered Agent',
            company_address     : '1 Cambridge St',
            company_suite       : '#607',
            company_city        : 'Boston',
            company_state       : 'MA',
            company_zipcode     : '01111',
            company_email       : 'info@northwestregsistered.com',
            company_phone       : '607-411-4141',
            company_fax         : '607-411-4142',
            company_www         : 'http://www.northwestregisteredagent.com/',
            company_logo        : '/assets/website-manager-logos/site_1_nwra.jpg',
            color_header        : '#feffef',
            color_text          : '#e45e4a',
            color_link          : '#887a6a',
            chat_link           : '<script type=text/javascript>SAMPLE</script>',
            billing_address     : '12 Hull Pl',
            billing_suite       : '#1212',
            billing_city        : 'Boston',
            billing_state       : 'MA',
            billing_zipcode     : '01111',
            billing_country     : 'US',
            billing_email       : 'info@massregagent.com',
            billing_phone       : '607-411-4141',
            billing_fax         : '607-411-5151',
            net_billing         : '30',
            checkout_method     : 'Stripe',
            renewal_factoring   : '9',
            live                : '1'
        },{
            id                  : 'bama',
            company_name        : 'Alabama Registered Agent',
            company_address     : '1 Cambridge St',
            company_suite       : '#607',
            company_city        : 'Montgomery',
            company_state       : 'AL',
            company_zipcode     : '01111',
            company_country     : 'US',
            company_email       : 'info@alaregagent.com',
            company_phone       : '607-411-4141',
            company_fax         : '607-411-4142',
            company_www         : 'http://alaregagent.com',
            company_logo        : '/assets/website-manager-logos/site_2_logo2.png',
            color_header        : '#feffef',
            color_text          : '#e45e4a',
            color_link          : '#887a6a',
            chat_link           : '<script type=text/javascript>SAMPLE</script>',
            billing_address     : '12 Hull Pl',
            billing_suite       : '#1212',
            billing_city        : 'Boston',
            billing_state       : 'MA',
            billing_zipcode     : '01111',
            billing_country     : 'US',
            billing_email       : 'info@massregagent.com',
            billing_phone       : '607-411-4141',
            billing_fax         : '607-411-4142',
            net_billing         : '30',
            checkout_method     : 'Stripe',
            renewal_factoring   : '9',
            live                : '1'
        },{
            id                  : 'wis-reg',
            company_name        : 'Wisconsin Registered Agent',
            company_address     : '1 Cambridge St',
            company_suite       : '#607',
            company_city        : 'Madison',
            company_state       : 'WI',
            company_zipcode     : '01111',
            company_country     : 'US',
            company_email       : 'info@wissregagent.com',
            company_phone       : '607-411-4141',
            company_fax         : '607-411-4142',
            company_www         : 'http://wissregagent.com',
            company_logo        : 'some_logo.jpg',
            color_header        : '#feffef',
            color_text          : '#e45e4a',
            color_link          : '#887a6a',
            chat_link           : '<script type=text/javascript>SAMPLE</script>',
            billing_address     : '12 Hull Pl',
            billing_suite       : '#1212',
            billing_city        : 'Boston',
            billing_state       : 'MA',
            billing_zipcode     : '01111',
            billing_country     : 'US',
            billing_email       : 'info@wissregagent.com',
            billing_phone       : '607-411-4141',
            billing_fax         : '607-411-4142',
            net_billing         : '30',
            checkout_method     : 'Stripe',
            renewal_factoring   : '9',
            live                : '1'
        },{
            id                  : 'pluto',
            company_name        : 'Pluto Registered Agent',
            company_address     : '1 Charon Way',
            company_suite       : '#607',
            company_city        : 'Plutoville',
            company_state       : 'HI',
            company_zipcode     : '01111',
            company_country     : 'US',
            company_email       : 'info@massregagent.com',
            company_phone       : '607-411-4141',
            company_fax         : '607-411-4142',
            company_www         : 'http://plutoregagent.com',
            company_logo        : '',
            color_header        : '#feffef',
            color_text          : '#e45e4a',
            color_link          : '#887a6a',
            chat_link           : '<script type=text/javascript>SAMPLE</script>',
            billing_address     : '12 Hull Pl',
            billing_suite       : '#1212',
            billing_city        : 'Boston',
            billing_state       : 'MA',
            billing_zipcode     : '01111',
            billing_country     : 'US',
            billing_email       : 'info@massregagent.com',
            billing_phone       : '607-411-4141',
            billing_fax         : '607-411-4142',
            net_billing         : '30',
            checkout_method     : 'Stripe',
            renewal_factoring   : '9',
            live                : '0'
        }
		];

		return websites;

	}
});
