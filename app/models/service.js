var Services = Ember.Object.extend({


});

Services.reopenClass({

	getServices: function() {

		var allServices = [

			{"id": "1", "routeName":"",			"routeLabel":"Website Manager"},
			{"id": "2", "routeName":"",			"routeLabel":"Place Manager"},
			{"id": "3", "routeName":"",			"routeLabel":"Agency Manager"},
			{"id": "4", "routeName":"",			"routeLabel":"Resource Manager"},
			{"id": "5", "routeName":"",			"routeLabel":"Registered Agent Manager"},
			{"id": "6", "routeName":"",			"routeLabel":"Product Manager"},
			{"id": "7", "routeName":"",			"routeLabel":"Document Manager"}

		];

		return allServices;
	}


});


export default Services;

/*

			var serviceObj = [];
			$.getJSON("/assets/json/services.json").then(function(servDat) {
				console.log("~~~~~~~~~~~~~ JSON COMPLETED");
				servDat.services.forEach(function(data) {
					console.log("~~~~~~~~~~~~~~~~ "+data.routeLabel);
					serviceObj.pushObject(data);
				});
			});
			return serviceObj;

*/
