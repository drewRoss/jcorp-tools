var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
	this.route('component-test');
	this.route('helper-test');

	this.route('index', { path: '/'} );
	this.route('manage', { path: '/manage' });
	this.resource('manage', function () {
		this.resource('website', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('agency', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('document', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('entity', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('place', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('registered-agent', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
		this.resource('resource', function () {
			this.route('edit', { path: '/:id' });
			this.route('add');
		});
	});
});

export default Router;